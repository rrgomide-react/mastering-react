import React, { Component, Image } from 'react';
import Title from './Title';
import Description from './Description';
import './css/NewsItem.css';

const styles = {
  teste: {
    flex: 1,
    flexDirection: 'row'
  }
}

export default class NewsItem extends Component {

  render() {
    return (
      <div className='newsitem-item'>
        <div className='newsitem-cover'>
          <img src={this.props.image} />
        </div>
        <div className='newsitem-data'>
          <Description><strong>Título: </strong>{this.props.title}</Description>
          <Description><strong>Autor: </strong>{this.props.author}</Description>
          <Description><strong>Páginas: </strong>{this.props.numberOfPages}</Description>
          <Description><strong>Publicação: </strong>{this.props.publishedDate}</Description>
        </div>
      </div>
    )
  }
}
