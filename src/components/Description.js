import React,  { Component } from 'react';

export default class Description extends Component {

  render() {
    return (
      <p style={{margin: 0}}>{this.props.children}</p>
    );
  }
}