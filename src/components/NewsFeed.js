import React, { Component } from 'react';
import NewsItem from './NewsItem';
import './css/NewsFeed.css';

class NewsFeed extends Component {

  constructor() {
    super();
    this.state = {
      dados: null
    }
  }

  componentWillMount() {

    fetch('https://www.googleapis.com/books/v1/volumes?q=deitel')
      .then(response => response.json())
      .then(json => this.setState({dados: json}));
  }

  render() {

    if(!this.state.dados)
      return null;

    return (
      <div className='contorno'>
        {
          this.state.dados.items.map(item =>
            <NewsItem
              key={item.id}
              image={item.volumeInfo.imageLinks.thumbnail}
              description={item.volumeInfo.description}
              numberOfPages={item.volumeInfo.pageCount}
              publishedDate={item.volumeInfo.publishedDate}
              title={item.volumeInfo.title}
              author={item.volumeInfo.authors[0]}
            />
          )
        }
      </div>
    );
  }
}

export default NewsFeed;
