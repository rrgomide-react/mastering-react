import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './css/Title.css';

export default class Title extends Component {

  render() {
    return (
      <div>
        <p
          className='title-titulo'
          style={{ backgroundColor: this.props.highlighted ? 'yellow' : 'white'}}
        >
          {this.props.children}
        </p>
      </div>
    );
  }
}

Title.propTypes = {
  titleText: PropTypes.string,
  highlighted: PropTypes.bool,
  fontSize: PropTypes.number
};
