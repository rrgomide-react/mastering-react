import React, { Component } from 'react';
import NewsFeed from './components/NewsFeed';

import './css/App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NewsFeed>
        </NewsFeed>
      </div>
    );
  }
}

/*
<header className="App-header">
  <img src={logo} className="App-logo" alt="logo" />
  <h1 className="App-title">Welcome to React, Raphael!</h1>
</header>
<p className="App-intro">
  To get started, edit <code>src/App.js</code> and save to reload.
</p>
*/

export default App;
